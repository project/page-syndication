<?php

/**
 * Menu callback for admin/settings/page_syndication/referer_blacklist.
 *
 * @return array
 */
function page_syndication_referer_blacklist_settings() {
  $headers = array(
                  'referer' => array('data' => t('Referer'), 'width' => 200),
                  'action' => array('data' => t('Operations'))
                  );

  $results = pager_query('SELECT * FROM {page_syndication_referer_blacklist}', 30);

  $i = 1;
  while ($result = db_fetch_array($results)) {
    
    $data[$i] = $result;
    $data[$i]['action'] = _generate_options($result['id']);
    unset($data[$i]['id']);
    $i++;
  }

  $table_output = theme('admin_links');
  $table_output .= theme('table', $headers, $data);
  $table_output .= theme('pager', NULL, 30, 0);

  return $table_output;
}

function _generate_options($id) {
  return l(t('Remove'), 'admin/settings/page_syndication/referer_blacklist/remove/'.$id);
}

function page_syndication_remove_referer_confirm(&$form_state, $url_id) {
  $form['page_syndication_url_id'] = array(
    '#type' => 'value',
    '#value' => $url_id,
  );

  $url = db_query('SELECT * FROM {page_syndication_referer_blacklist} WHERE id=%d', $url_id);
  $url = db_fetch_array($url);

  return confirm_form($form,
    t('Are you sure you want to remove %url from the blacklist?', array('%url' => $url['url'])),
    'admin/settings/page_syndication/referer_blacklist',
    t('This action cannot be reverted.'),
    t('Remove'),
    t('Cancel')
  );
}

function page_syndication_remove_referer_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    db_query('DELETE FROM {page_syndication_referer_blacklist} WHERE id=%d', $form_state['values']['page_syndication_url_id']);
  }

  $form_state['redirect'] = 'admin/settings/page_syndication/referer_blacklist';
}

function page_syndication_add_referer_settings() {
  $form['referer_url'] = array(
    '#type' => 'textfield',
    '#size' => 80,
    '#description' => 'The url should be in the form of http://www.example.com',
    '#maxlength' => 1000
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Add'));
  $form['cancel'] = array('#type' => 'markup', '#value' => l(t('Cancel'), 'admin/settings/page_syndication/referer_blacklist'));

  return $form;
}

function page_syndication_add_referer_settings_validate($form, &$form_state) {
  if (!valid_url($form_state['values']['referer_url'], true)) {
    form_set_error('', t('The provided url is invalid. Correct format is http://www.example.com'));
  }
}

function page_syndication_add_referer_settings_submit($form, &$form_state) {
  db_query('INSERT INTO {page_syndication_referer_blacklist}(url) VALUES ("%s")', $form_state['values']['referer_url']);

  drupal_goto('admin/settings/page_syndication/referer_blacklist');
}

function page_syndication_settings_form() {
  $form = array();

  $form['css_information'] = array(
    '#value' => '<h4>CSS Blacklist:</h4>',
  );
  $form['removed_css'] = array(
    '#type' => 'textarea',
    '#default_value' => variable_get('page_syndication_removed_css', ''),
    '#rows' => 10,
    '#cols' => 6,
    '#description' => t('Some core Drupal CSS stylesheets interfere with third party sites. By entering the file on this list you can avoid loading it. <br/> Eg: modules/node/node.css')
  );
  
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  
  return $form;
}

function page_syndication_settings_form_submit($form, &$form_state) {
  variable_set('page_syndication_removed_css', $form_state['values']['removed_css']);
  
  drupal_goto('admin/settings/page_syndication');
}