Page Syndication
----------------	

Project page at: http://drupal.org/project/page-syndication

Installation
------------

To install, place the entire Page Syndication folder into your modules directory.
Go to Administer -> Site building -> Modules and enable the Page Syndication module.

Usage
-----

Page Syndication is integrated with the CTools Page Manager module. In order to use the embed system,
you must create a Panel Page, and then get the embed code from the Page Syndication -> Embed tab
in the Page Manager edit page, page.

For other module settings, please go to Administer -> Site Configuration -> Page Syndication Settings.

Maintainers
-----------

Tudor Sitaru